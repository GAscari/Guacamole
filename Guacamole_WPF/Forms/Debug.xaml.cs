﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Guacamole_WPF.Classes;

namespace Guacamole_WPF.Forms
{
    /// <summary>
    /// Logica di interazione per Debug.xaml
    /// </summary>
    public partial class Debug : Window
    {
        public Debug(Session session)
        {
            InitializeComponent();
            lvDataSet.ItemsSource = session.DataSet;
            txtFileName.Text = session.FileName;
        }
    }
}
