﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Guacamole_WPF.Forms
{
    /// <summary>
    /// Logica di interazione per Message.xaml
    /// </summary>
    public partial class Message : Window
    {
        public Message(string title, string message)
        {
            InitializeComponent();
            this.Title = title;
            messageLbl.Content = message;
        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public static class MessageForm
    {
        public static void Show(string title, string message)
        {
            (new Message(title, message)).ShowDialog();
        }

        public static void Show(string message)
        {
            (new Message("Message", message)).ShowDialog();
        }

        public static void Show()
        {
            (new Message("Message", "Dunno y bro")).ShowDialog();
        }
    }
}
