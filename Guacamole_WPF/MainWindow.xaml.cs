﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Guacamole_WPF.Classes;
using Guacamole_WPF.Forms;
using Microsoft.Win32;
using System.ComponentModel;
using System.Threading;

namespace Guacamole_WPF
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Session session = new Session();
        private OpenFileDialog openFileDialog = new OpenFileDialog();
        private SaveFileDialog saveFileDialog = new SaveFileDialog();

        public Session Session { get => session; set { session = value; NotifyPropertyChanged("Session"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            openFileDialog.Filter = "Guacamole session | *.gcml";
            saveFileDialog.Filter = "Guacamole session | *.gcml";
        }

        private void mtSaveSession()
        {
            Session.Save(session);
            Thread.Sleep(1000);
        }

        private void menuNewSession(object sender, RoutedEventArgs e)
        {
            if (Session.Writable(session) || session.DataSet.Count > 0)
            {
                if (MessageBox.Show("Are you sure to create a new session? Any unsaved change will be lost", "New session", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    this.Session = new Session();
                }
            }
        }
        private void menuSaveSession(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Session.Writable(session))
                {
                    //Session.Save(session);
                    Thread thread = new Thread(mtSaveSession);
                    this.IsEnabled = false;
                    thread.Start();
                    thread.Join();
                    this.IsEnabled = true;
                }
                else
                {
                    saveFileDialog.FileName = "";
                    if ((bool)saveFileDialog.ShowDialog())
                    {
                        Session new_session = Session.Parse(this.session.ToString());
                        new_session.FileName = saveFileDialog.FileName;
                        Session.Save(new_session);
                        this.Session = new_session;
                    }
                }
            }
            catch
            {
                MessageForm.Show("ERROR");
            }
        }
        private void menuSaveAsSession(object sender, RoutedEventArgs e)
        {
            try
            {
                saveFileDialog.FileName = "";
                if ((bool)saveFileDialog.ShowDialog())
                {
                    Session new_session = Session.Parse(this.session.ToString());
                    new_session.FileName = saveFileDialog.FileName;
                    Session.Save(new_session);
                    this.Session = new_session;
                }
            }
            catch
            {
                MessageForm.Show("ERROR");
            }
        }
        private void menuLoadSession(object sender, RoutedEventArgs e)
        {
            try
            {
                openFileDialog.FileName = "";
                if ((bool)openFileDialog.ShowDialog())
                {
                    Session new_session = Session.Load(openFileDialog.FileName);
                    if  (new_session != null)
                    {
                        this.Session = new_session;
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
            }
            catch
            {
                MessageForm.Show("ERROR");
            }
            
        }
        private void menuExitSession(object sender, RoutedEventArgs e)
        {
            if (session.FileName.Length > 0 || session.DataSet.Count > 0)
            {
                if (MessageBox.Show("Are you sure to exit Guacamole? Any unsaved change will be lost", "Exit", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
                {
                    this.Close();
                }
            }
            else
            {
                this.Close();
            }
        }

        private void menuDebug(object sender, RoutedEventArgs e)
        {
            Forms.Debug debug = new Debug(session);
            debug.ShowDialog();
        }

        private void menuTest(object sender, RoutedEventArgs e)
        {
            try
            {
                bool first = true;
                string list = "None of the data have been compressed.";
                Session newSession = this.Session;
                foreach (Data data in newSession.DataSet)
                {
                    if (data.Compress())
                    {
                        if (first)
                        {
                            list = "The following data have been compressed:\n";
                        }
                        first = false;
                        list += data.Name + " - " + data.Compression + "\n";
                    }
                }
                this.Session = Session.Parse(newSession.ToString());
                MessageForm.Show(list);
            }
            catch
            {
                MessageForm.Show("ERROR");
            }
        }
    }
}
