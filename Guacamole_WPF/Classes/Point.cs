﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole_WPF.Classes
{
    public class Point
    {
        private double x;
        private double y;

        public Point()
        {

        }

        public Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }

        public override string ToString()
        {
            return String.Format("{0}/{1}", x, y);
        }

        public static bool TryParse(string text, out Point point)
        {
            try
            {
                string[] s = text.Split('/');
                List<double> list = new List<double>();
                point = new Point(Double.Parse(s[0]), Double.Parse(s[1]));
                return true;
            }
            catch
            {
                point = null;
                return false;
            }
        }

        public static Point Parse(string text)
        {
            Point point = null;
            if (TryParse(text, out point))
            {
                return point;
            }
            else
            {
                throw new FormatException("Invalid format for Point object");
            }
        }
    }
}
