﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole_WPF.Classes
{
    public class DataSet : List<Data>
    {
        public DataSet() { }

        public int MaxCount
        {
            get
            {
                int max = 0;
                foreach (Data data in this)
                {
                    if (data.Count > max) max = data.Count;
                }
                return max;
            }
        }
        public int VisibleMaxCount
        {
            get
            {
                int max = 0;
                foreach (Data data in this)
                {
                    if (data.Visible && data.Count > max) max = data.Count;
                }
                return max;
            }
        }

        public DataSet(List<Data> list)
        {
            this.AddRange(list);
        }

        public static DataSet Parse(string text)
        {
            DataSet dataSet = new DataSet();
            if (TryParse(text, out dataSet))
            {
                return dataSet;
            }
            else
            {
                return null;
            }
        }

        public static bool TryParse(string text, out DataSet dataSet)
        {
            dataSet = new DataSet();
            try
            {
                string[] v = text.Trim().Split('\n');
                foreach (string item in v)
                {
                    dataSet.Add(Data.Parse(item));
                }
                return true;
            }
            catch
            {
                dataSet = null;
                return false;
            }
        }

        public override string ToString()
        {
            string text = "";
            for (int i = 0; i < this.Count; i++)
            {
                text += this[i].ToString();
                if (i != this.Count - 1)
                {
                    text += "\n";
                }
            }
            return text;
        }
    }
}
