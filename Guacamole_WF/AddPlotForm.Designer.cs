﻿namespace Guacamole_WF
{
    partial class AddPlotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddPlotForm));
            this.label1 = new System.Windows.Forms.Label();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.colorBtn = new System.Windows.Forms.Button();
            this.visibleCheck = new System.Windows.Forms.CheckBox();
            this.xBtn = new System.Windows.Forms.Button();
            this.yBtn = new System.Windows.Forms.Button();
            this.xLabel = new System.Windows.Forms.Label();
            this.yLabel = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(13, 30);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(285, 20);
            this.nameTxt.TabIndex = 1;
            this.nameTxt.TextChanged += new System.EventHandler(this.plot_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Color";
            // 
            // colorBtn
            // 
            this.colorBtn.Location = new System.Drawing.Point(13, 74);
            this.colorBtn.Name = "colorBtn";
            this.colorBtn.Size = new System.Drawing.Size(285, 23);
            this.colorBtn.TabIndex = 3;
            this.colorBtn.UseVisualStyleBackColor = true;
            this.colorBtn.Click += new System.EventHandler(this.colorBtn_Click);
            this.colorBtn.Paint += new System.Windows.Forms.PaintEventHandler(this.colorBtn_Paint);
            // 
            // visibleCheck
            // 
            this.visibleCheck.AutoSize = true;
            this.visibleCheck.Checked = true;
            this.visibleCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.visibleCheck.Location = new System.Drawing.Point(13, 104);
            this.visibleCheck.Name = "visibleCheck";
            this.visibleCheck.Size = new System.Drawing.Size(56, 17);
            this.visibleCheck.TabIndex = 4;
            this.visibleCheck.Text = "Visible";
            this.visibleCheck.UseVisualStyleBackColor = true;
            this.visibleCheck.CheckedChanged += new System.EventHandler(this.plot_TextChanged);
            // 
            // xBtn
            // 
            this.xBtn.Location = new System.Drawing.Point(13, 128);
            this.xBtn.Name = "xBtn";
            this.xBtn.Size = new System.Drawing.Size(110, 23);
            this.xBtn.TabIndex = 5;
            this.xBtn.Text = "Select x axis";
            this.xBtn.UseVisualStyleBackColor = true;
            this.xBtn.Click += new System.EventHandler(this.xBtn_Click);
            // 
            // yBtn
            // 
            this.yBtn.Location = new System.Drawing.Point(13, 158);
            this.yBtn.Name = "yBtn";
            this.yBtn.Size = new System.Drawing.Size(110, 23);
            this.yBtn.TabIndex = 6;
            this.yBtn.Text = "Select y axis";
            this.yBtn.UseVisualStyleBackColor = true;
            this.yBtn.Click += new System.EventHandler(this.yBtn_Click);
            // 
            // xLabel
            // 
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(129, 133);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(96, 13);
            this.xLabel.TabIndex = 7;
            this.xLabel.Text = "No x data selected";
            this.xLabel.TextChanged += new System.EventHandler(this.plot_TextChanged);
            // 
            // yLabel
            // 
            this.yLabel.AutoSize = true;
            this.yLabel.Location = new System.Drawing.Point(129, 163);
            this.yLabel.Name = "yLabel";
            this.yLabel.Size = new System.Drawing.Size(96, 13);
            this.yLabel.TabIndex = 8;
            this.yLabel.Text = "No y data selected";
            this.yLabel.TextChanged += new System.EventHandler(this.plot_TextChanged);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(223, 215);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 9;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(142, 215);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 10;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // colorDialog
            // 
            this.colorDialog.SolidColorOnly = true;
            // 
            // AddPlotForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 250);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.yLabel);
            this.Controls.Add(this.xLabel);
            this.Controls.Add(this.yBtn);
            this.Controls.Add(this.xBtn);
            this.Controls.Add(this.visibleCheck);
            this.Controls.Add(this.colorBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddPlotForm";
            this.Text = "Add a new plot";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button colorBtn;
        private System.Windows.Forms.CheckBox visibleCheck;
        private System.Windows.Forms.Button xBtn;
        private System.Windows.Forms.Button yBtn;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label yLabel;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.ColorDialog colorDialog;
    }
}