﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Guacamole_WF
{

    public class Plot 
    {
        private string name;
        private string listX;
        private string listY;
        private Color color;
        private bool visible;

        public string Name { get => name; set => name = value; }
        public string ListX { get => listX; set => listX = value; }
        public string ListY { get => listY; set => listY = value; }
        public Color Color { get => color; set => color = value; }
        public bool Visible { get => visible; set => visible = value; }

        public Plot() { }

        public Plot(string name, string listX, string listY, Color color, bool visible)
        {
            this.name = name;
            this.listX = listX;
            this.listY = listY;
            this.color = color;
            this.visible = visible;
        }

        public static Plot Parse(string text)
        {
            Plot plot = new Plot();
            if (TryParse(text, out plot))
            {
                return plot;
            }
            else
            {
                throw new FormatException("String format error");
            }
        }

        public static bool TryParse(string text, out Plot plot)
        {
            plot = new Plot();
            try
            {
                string[] v = text.Split(';');
                plot = new Plot(v[0], v[1], v[2], Color.FromArgb(int.Parse(v[3])), Boolean.Parse(v[4]));
                return true;
            }
            catch
            {
                plot = null;
                return false;
            }
        }

        public override string ToString()
        {
            return string.Format("{0};{1};{2};{3};{4}", name, listX, listY, color.ToArgb(), visible);
        }
    }
}
