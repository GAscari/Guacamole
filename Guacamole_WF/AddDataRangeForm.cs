﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class AddDataRangeForm : Form
    {
        private List<double> data;

        public List<double> Data { get => data; }

        public AddDataRangeForm()
        {
            InitializeComponent();
        }
        private void range_TextChanged(object sender, EventArgs e)
        {
            //int first = 0, step = 0, count = 0;
            //if (Int32.TryParse(firstNum.Value.ToString(), out first) && Int32.TryParse(stepNum.Value.ToString(), out step) && Int32.TryParse(countNum.Value.ToString(), out count) && count > 0)
            if (countNum.Value > 0)
            {
                okBtn.Enabled = true;
            }
            else
            {
                okBtn.Enabled = false;
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            try
            {
                data = new List<double>();
                for (double i = 0; i < (double)countNum.Value; i++)
                {
                    data.Add(i * (double)stepNum.Value + (double)firstNum.Value);
                }
            }
            catch
            {
                data = null;
            }
        }
    }
}
