﻿namespace Guacamole_WF
{
    partial class ElaborateDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ElaborateDataForm));
            this.averageGBox = new System.Windows.Forms.GroupBox();
            this.averageKeepCBox = new System.Windows.Forms.CheckBox();
            this.averageSpanNum = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.averageRBtn = new System.Windows.Forms.RadioButton();
            this.deriveGBox = new System.Windows.Forms.GroupBox();
            this.deriveKeepCBox = new System.Windows.Forms.CheckBox();
            this.deriveSpanNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.deriveRBtn = new System.Windows.Forms.RadioButton();
            this.boundGBox = new System.Windows.Forms.GroupBox();
            this.boundLowerNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.boundUpperNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.boundRBtn = new System.Windows.Forms.RadioButton();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.selectBtn = new System.Windows.Forms.Button();
            this.selectLbl = new System.Windows.Forms.Label();
            this.copyCBox = new System.Windows.Forms.CheckBox();
            this.ratioGBox = new System.Windows.Forms.GroupBox();
            this.ratioCustomNum = new System.Windows.Forms.NumericUpDown();
            this.ratioCustomRBtn = new System.Windows.Forms.RadioButton();
            this.ratioLastRBtn = new System.Windows.Forms.RadioButton();
            this.ratioFirstRBtn = new System.Windows.Forms.RadioButton();
            this.ratioRBtn = new System.Windows.Forms.RadioButton();
            this.averageGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.averageSpanNum)).BeginInit();
            this.deriveGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deriveSpanNum)).BeginInit();
            this.boundGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boundLowerNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boundUpperNum)).BeginInit();
            this.ratioGBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratioCustomNum)).BeginInit();
            this.SuspendLayout();
            // 
            // averageGBox
            // 
            this.averageGBox.Controls.Add(this.averageKeepCBox);
            this.averageGBox.Controls.Add(this.averageSpanNum);
            this.averageGBox.Controls.Add(this.label1);
            this.averageGBox.Controls.Add(this.averageRBtn);
            this.averageGBox.Location = new System.Drawing.Point(12, 81);
            this.averageGBox.Name = "averageGBox";
            this.averageGBox.Size = new System.Drawing.Size(379, 55);
            this.averageGBox.TabIndex = 5;
            this.averageGBox.TabStop = false;
            this.averageGBox.Text = " ";
            // 
            // averageKeepCBox
            // 
            this.averageKeepCBox.AutoSize = true;
            this.averageKeepCBox.Checked = true;
            this.averageKeepCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.averageKeepCBox.Location = new System.Drawing.Point(238, 26);
            this.averageKeepCBox.Name = "averageKeepCBox";
            this.averageKeepCBox.Size = new System.Drawing.Size(135, 17);
            this.averageKeepCBox.TabIndex = 4;
            this.averageKeepCBox.Text = "Keep number of values";
            this.averageKeepCBox.UseVisualStyleBackColor = true;
            // 
            // averageSpanNum
            // 
            this.averageSpanNum.Location = new System.Drawing.Point(44, 23);
            this.averageSpanNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.averageSpanNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.averageSpanNum.Name = "averageSpanNum";
            this.averageSpanNum.Size = new System.Drawing.Size(188, 20);
            this.averageSpanNum.TabIndex = 3;
            this.averageSpanNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Span";
            // 
            // averageRBtn
            // 
            this.averageRBtn.AutoSize = true;
            this.averageRBtn.Location = new System.Drawing.Point(6, 0);
            this.averageRBtn.Name = "averageRBtn";
            this.averageRBtn.Size = new System.Drawing.Size(155, 17);
            this.averageRBtn.TabIndex = 1;
            this.averageRBtn.Text = "Average on defined interval";
            this.averageRBtn.UseVisualStyleBackColor = true;
            this.averageRBtn.CheckedChanged += new System.EventHandler(this.elaborateRBtn_CheckedChanged);
            // 
            // deriveGBox
            // 
            this.deriveGBox.Controls.Add(this.deriveKeepCBox);
            this.deriveGBox.Controls.Add(this.deriveSpanNum);
            this.deriveGBox.Controls.Add(this.label2);
            this.deriveGBox.Controls.Add(this.deriveRBtn);
            this.deriveGBox.Location = new System.Drawing.Point(12, 142);
            this.deriveGBox.Name = "deriveGBox";
            this.deriveGBox.Size = new System.Drawing.Size(379, 55);
            this.deriveGBox.TabIndex = 6;
            this.deriveGBox.TabStop = false;
            this.deriveGBox.Text = " ";
            // 
            // deriveKeepCBox
            // 
            this.deriveKeepCBox.AutoSize = true;
            this.deriveKeepCBox.Checked = true;
            this.deriveKeepCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.deriveKeepCBox.Location = new System.Drawing.Point(238, 26);
            this.deriveKeepCBox.Name = "deriveKeepCBox";
            this.deriveKeepCBox.Size = new System.Drawing.Size(135, 17);
            this.deriveKeepCBox.TabIndex = 4;
            this.deriveKeepCBox.Text = "Keep number of values";
            this.deriveKeepCBox.UseVisualStyleBackColor = true;
            // 
            // deriveSpanNum
            // 
            this.deriveSpanNum.Location = new System.Drawing.Point(44, 23);
            this.deriveSpanNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.deriveSpanNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.deriveSpanNum.Name = "deriveSpanNum";
            this.deriveSpanNum.Size = new System.Drawing.Size(188, 20);
            this.deriveSpanNum.TabIndex = 3;
            this.deriveSpanNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Span";
            // 
            // deriveRBtn
            // 
            this.deriveRBtn.AutoSize = true;
            this.deriveRBtn.Location = new System.Drawing.Point(6, 0);
            this.deriveRBtn.Name = "deriveRBtn";
            this.deriveRBtn.Size = new System.Drawing.Size(146, 17);
            this.deriveRBtn.TabIndex = 1;
            this.deriveRBtn.TabStop = true;
            this.deriveRBtn.Text = "Derive on defined interval";
            this.deriveRBtn.UseVisualStyleBackColor = true;
            this.deriveRBtn.CheckedChanged += new System.EventHandler(this.elaborateRBtn_CheckedChanged);
            // 
            // boundGBox
            // 
            this.boundGBox.Controls.Add(this.boundLowerNum);
            this.boundGBox.Controls.Add(this.label4);
            this.boundGBox.Controls.Add(this.boundUpperNum);
            this.boundGBox.Controls.Add(this.label3);
            this.boundGBox.Controls.Add(this.boundRBtn);
            this.boundGBox.Location = new System.Drawing.Point(12, 203);
            this.boundGBox.Name = "boundGBox";
            this.boundGBox.Size = new System.Drawing.Size(379, 55);
            this.boundGBox.TabIndex = 7;
            this.boundGBox.TabStop = false;
            this.boundGBox.Text = " ";
            // 
            // boundLowerNum
            // 
            this.boundLowerNum.DecimalPlaces = 6;
            this.boundLowerNum.Location = new System.Drawing.Point(238, 23);
            this.boundLowerNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.boundLowerNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.boundLowerNum.Name = "boundLowerNum";
            this.boundLowerNum.Size = new System.Drawing.Size(120, 20);
            this.boundLowerNum.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(196, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Lower";
            // 
            // boundUpperNum
            // 
            this.boundUpperNum.DecimalPlaces = 6;
            this.boundUpperNum.Location = new System.Drawing.Point(48, 23);
            this.boundUpperNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.boundUpperNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.boundUpperNum.Name = "boundUpperNum";
            this.boundUpperNum.Size = new System.Drawing.Size(120, 20);
            this.boundUpperNum.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Upper";
            // 
            // boundRBtn
            // 
            this.boundRBtn.AutoSize = true;
            this.boundRBtn.Location = new System.Drawing.Point(6, 0);
            this.boundRBtn.Name = "boundRBtn";
            this.boundRBtn.Size = new System.Drawing.Size(170, 17);
            this.boundRBtn.TabIndex = 1;
            this.boundRBtn.TabStop = true;
            this.boundRBtn.Text = "Bound to max. and min. values";
            this.boundRBtn.UseVisualStyleBackColor = true;
            this.boundRBtn.CheckedChanged += new System.EventHandler(this.elaborateRBtn_CheckedChanged);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(316, 362);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 8;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(235, 362);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 9;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // selectBtn
            // 
            this.selectBtn.Location = new System.Drawing.Point(13, 13);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(112, 23);
            this.selectBtn.TabIndex = 10;
            this.selectBtn.Text = "Select data/s";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.selectBtn_Click);
            // 
            // selectLbl
            // 
            this.selectLbl.AutoSize = true;
            this.selectLbl.Location = new System.Drawing.Point(131, 18);
            this.selectLbl.Name = "selectLbl";
            this.selectLbl.Size = new System.Drawing.Size(88, 13);
            this.selectLbl.TabIndex = 11;
            this.selectLbl.Text = "No data selected";
            // 
            // copyCBox
            // 
            this.copyCBox.AutoSize = true;
            this.copyCBox.Checked = true;
            this.copyCBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.copyCBox.Location = new System.Drawing.Point(13, 43);
            this.copyCBox.Name = "copyCBox";
            this.copyCBox.Size = new System.Drawing.Size(137, 17);
            this.copyCBox.TabIndex = 12;
            this.copyCBox.Text = "Create copy of the data";
            this.copyCBox.UseVisualStyleBackColor = true;
            // 
            // ratioGBox
            // 
            this.ratioGBox.Controls.Add(this.ratioCustomNum);
            this.ratioGBox.Controls.Add(this.ratioCustomRBtn);
            this.ratioGBox.Controls.Add(this.ratioLastRBtn);
            this.ratioGBox.Controls.Add(this.ratioFirstRBtn);
            this.ratioGBox.Controls.Add(this.ratioRBtn);
            this.ratioGBox.Location = new System.Drawing.Point(12, 264);
            this.ratioGBox.Name = "ratioGBox";
            this.ratioGBox.Size = new System.Drawing.Size(379, 81);
            this.ratioGBox.TabIndex = 8;
            this.ratioGBox.TabStop = false;
            this.ratioGBox.Text = " ";
            // 
            // ratioCustomNum
            // 
            this.ratioCustomNum.DecimalPlaces = 6;
            this.ratioCustomNum.Location = new System.Drawing.Point(175, 24);
            this.ratioCustomNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.ratioCustomNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.ratioCustomNum.Name = "ratioCustomNum";
            this.ratioCustomNum.Size = new System.Drawing.Size(120, 20);
            this.ratioCustomNum.TabIndex = 5;
            // 
            // ratioCustomRBtn
            // 
            this.ratioCustomRBtn.AutoSize = true;
            this.ratioCustomRBtn.Location = new System.Drawing.Point(108, 24);
            this.ratioCustomRBtn.Name = "ratioCustomRBtn";
            this.ratioCustomRBtn.Size = new System.Drawing.Size(60, 17);
            this.ratioCustomRBtn.TabIndex = 4;
            this.ratioCustomRBtn.Text = "Custom";
            this.ratioCustomRBtn.UseVisualStyleBackColor = true;
            this.ratioCustomRBtn.CheckedChanged += new System.EventHandler(this.ratioRBtn_CheckedChanged);
            // 
            // ratioLastRBtn
            // 
            this.ratioLastRBtn.AutoSize = true;
            this.ratioLastRBtn.Location = new System.Drawing.Point(9, 47);
            this.ratioLastRBtn.Name = "ratioLastRBtn";
            this.ratioLastRBtn.Size = new System.Drawing.Size(74, 17);
            this.ratioLastRBtn.TabIndex = 3;
            this.ratioLastRBtn.Text = "Last value";
            this.ratioLastRBtn.UseVisualStyleBackColor = true;
            this.ratioLastRBtn.CheckedChanged += new System.EventHandler(this.ratioRBtn_CheckedChanged);
            // 
            // ratioFirstRBtn
            // 
            this.ratioFirstRBtn.AutoSize = true;
            this.ratioFirstRBtn.Checked = true;
            this.ratioFirstRBtn.Location = new System.Drawing.Point(9, 24);
            this.ratioFirstRBtn.Name = "ratioFirstRBtn";
            this.ratioFirstRBtn.Size = new System.Drawing.Size(73, 17);
            this.ratioFirstRBtn.TabIndex = 2;
            this.ratioFirstRBtn.TabStop = true;
            this.ratioFirstRBtn.Text = "First value";
            this.ratioFirstRBtn.UseVisualStyleBackColor = true;
            this.ratioFirstRBtn.CheckedChanged += new System.EventHandler(this.ratioRBtn_CheckedChanged);
            // 
            // ratioRBtn
            // 
            this.ratioRBtn.AutoSize = true;
            this.ratioRBtn.Location = new System.Drawing.Point(6, 0);
            this.ratioRBtn.Name = "ratioRBtn";
            this.ratioRBtn.Size = new System.Drawing.Size(100, 17);
            this.ratioRBtn.TabIndex = 1;
            this.ratioRBtn.Text = "Ratio to a value";
            this.ratioRBtn.UseVisualStyleBackColor = true;
            this.ratioRBtn.CheckedChanged += new System.EventHandler(this.elaborateRBtn_CheckedChanged);
            // 
            // ElaborateDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 397);
            this.Controls.Add(this.ratioGBox);
            this.Controls.Add(this.copyCBox);
            this.Controls.Add(this.selectLbl);
            this.Controls.Add(this.selectBtn);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.boundGBox);
            this.Controls.Add(this.deriveGBox);
            this.Controls.Add(this.averageGBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ElaborateDataForm";
            this.Text = "Elaborate existing data";
            this.averageGBox.ResumeLayout(false);
            this.averageGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.averageSpanNum)).EndInit();
            this.deriveGBox.ResumeLayout(false);
            this.deriveGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.deriveSpanNum)).EndInit();
            this.boundGBox.ResumeLayout(false);
            this.boundGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boundLowerNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boundUpperNum)).EndInit();
            this.ratioGBox.ResumeLayout(false);
            this.ratioGBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ratioCustomNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox averageKeepCBox;
        private System.Windows.Forms.NumericUpDown averageSpanNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton averageRBtn;
        private System.Windows.Forms.CheckBox deriveKeepCBox;
        private System.Windows.Forms.NumericUpDown deriveSpanNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton deriveRBtn;
        private System.Windows.Forms.NumericUpDown boundLowerNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown boundUpperNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton boundRBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button selectBtn;
        private System.Windows.Forms.Label selectLbl;
        private System.Windows.Forms.CheckBox copyCBox;
        private System.Windows.Forms.GroupBox averageGBox;
        private System.Windows.Forms.GroupBox deriveGBox;
        private System.Windows.Forms.GroupBox boundGBox;
        private System.Windows.Forms.GroupBox ratioGBox;
        private System.Windows.Forms.RadioButton ratioRBtn;
        private System.Windows.Forms.NumericUpDown ratioCustomNum;
        private System.Windows.Forms.RadioButton ratioCustomRBtn;
        private System.Windows.Forms.RadioButton ratioLastRBtn;
        private System.Windows.Forms.RadioButton ratioFirstRBtn;
    }
}