﻿namespace Guacamole_WF
{
    partial class AppendDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppendDataForm));
            this.selectBtn = new System.Windows.Forms.Button();
            this.dataLabel = new System.Windows.Forms.Label();
            this.functionRadio = new System.Windows.Forms.RadioButton();
            this.manipulationRadio = new System.Windows.Forms.RadioButton();
            this.rangeRadio = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.fileRadio = new System.Windows.Forms.RadioButton();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectBtn
            // 
            this.selectBtn.Location = new System.Drawing.Point(13, 13);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(112, 23);
            this.selectBtn.TabIndex = 0;
            this.selectBtn.Text = "Select data";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.selectBtn_Click);
            // 
            // dataLabel
            // 
            this.dataLabel.AutoSize = true;
            this.dataLabel.Location = new System.Drawing.Point(131, 18);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Size = new System.Drawing.Size(86, 13);
            this.dataLabel.TabIndex = 1;
            this.dataLabel.Text = "no data selected";
            this.dataLabel.TextChanged += new System.EventHandler(this.data_TextChanged);
            // 
            // functionRadio
            // 
            this.functionRadio.AutoSize = true;
            this.functionRadio.Enabled = false;
            this.functionRadio.Location = new System.Drawing.Point(15, 146);
            this.functionRadio.Name = "functionRadio";
            this.functionRadio.Size = new System.Drawing.Size(118, 17);
            this.functionRadio.TabIndex = 13;
            this.functionRadio.TabStop = true;
            this.functionRadio.Text = "Elementary function";
            this.functionRadio.UseVisualStyleBackColor = true;
            this.functionRadio.CheckedChanged += new System.EventHandler(this.data_TextChanged);
            // 
            // manipulationRadio
            // 
            this.manipulationRadio.AutoSize = true;
            this.manipulationRadio.Enabled = false;
            this.manipulationRadio.Location = new System.Drawing.Point(15, 122);
            this.manipulationRadio.Name = "manipulationRadio";
            this.manipulationRadio.Size = new System.Drawing.Size(159, 17);
            this.manipulationRadio.TabIndex = 12;
            this.manipulationRadio.TabStop = true;
            this.manipulationRadio.Text = "Manipulation of existing data";
            this.manipulationRadio.UseVisualStyleBackColor = true;
            this.manipulationRadio.CheckedChanged += new System.EventHandler(this.data_TextChanged);
            // 
            // rangeRadio
            // 
            this.rangeRadio.AutoSize = true;
            this.rangeRadio.Location = new System.Drawing.Point(15, 98);
            this.rangeRadio.Name = "rangeRadio";
            this.rangeRadio.Size = new System.Drawing.Size(146, 17);
            this.rangeRadio.TabIndex = 11;
            this.rangeRadio.TabStop = true;
            this.rangeRadio.Text = "Range with constant step";
            this.rangeRadio.UseVisualStyleBackColor = true;
            this.rangeRadio.CheckedChanged += new System.EventHandler(this.data_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "New data source:";
            // 
            // fileRadio
            // 
            this.fileRadio.AutoSize = true;
            this.fileRadio.Location = new System.Drawing.Point(15, 74);
            this.fileRadio.Name = "fileRadio";
            this.fileRadio.Size = new System.Drawing.Size(102, 17);
            this.fileRadio.TabIndex = 9;
            this.fileRadio.TabStop = true;
            this.fileRadio.Text = "From existing file";
            this.fileRadio.UseVisualStyleBackColor = true;
            this.fileRadio.CheckedChanged += new System.EventHandler(this.data_TextChanged);
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(115, 183);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 15;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(197, 183);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 14;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // AppendDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 217);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.functionRadio);
            this.Controls.Add(this.manipulationRadio);
            this.Controls.Add(this.rangeRadio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fileRadio);
            this.Controls.Add(this.dataLabel);
            this.Controls.Add(this.selectBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AppendDataForm";
            this.Text = "Append data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectBtn;
        private System.Windows.Forms.Label dataLabel;
        private System.Windows.Forms.RadioButton functionRadio;
        private System.Windows.Forms.RadioButton manipulationRadio;
        private System.Windows.Forms.RadioButton rangeRadio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton fileRadio;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}