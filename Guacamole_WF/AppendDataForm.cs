﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class AppendDataForm : Form
    {
        int index = -1;
        DataSet dataSet;

        public DataSet DataSet { get => dataSet; set => dataSet = value; }

        public AppendDataForm(DataSet dataSet)
        {
            this.dataSet = dataSet;
            InitializeComponent();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (fileRadio.Checked)
            {
                AddDataFileForm form = new AddDataFileForm(false);
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    if (form.DataList != null)
                    {
                        dataSet[index].AddRange(form.DataList[0]);
                    }
                }
            }
            else if (rangeRadio.Checked)
            {
                AddDataRangeForm form = new AddDataRangeForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    if (form.Data != null)
                    {
                        dataSet[index].AddRange(form.Data);
                    }
                }
            }
            else if (manipulationRadio.Checked)
            {

            }
            else if (functionRadio.Checked)
            {

            }
        }

        private void data_TextChanged(object sender, EventArgs e)
        {
            if (index > -1 && (fileRadio.Checked || rangeRadio.Checked || manipulationRadio.Checked || functionRadio.Checked))
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            SelectDataForm form = new SelectDataForm(dataSet, false);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                index = form.Index[0];
                dataLabel.Text = dataSet[index].Name;
            }
        }
    }
}
