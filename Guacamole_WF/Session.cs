﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole_WF
{
    class Session
    {
        private DataSet dataSet;
        private PlotSet plotSet;
        private string fileName;

        public Session(DataSet dataSet, PlotSet plotSet, string fileName) : this()
        {
            this.dataSet = dataSet;
            this.plotSet = plotSet;
            this.fileName = fileName;
        }

        public Session()
        {
            dataSet = new DataSet();
            plotSet = new PlotSet();
            fileName = "";
        }

        public DataSet DataSet { get => dataSet; set => dataSet = value; }
        public PlotSet PlotSet { get => plotSet; set => plotSet = value; }
        public string FileName { get => fileName; set => fileName = value; }

        public static Session Parse(string text)
        {
            Session session = new Session();
            if (TryParse(text, out session))
            {
                return session;
            }
            else
            {
                throw new FormatException("String format error");
            }
        }

        public static bool TryParse(string text, out Session session)
        {
            session = new Session();
            try
            {
                string[] v = text.Trim().Split('\n');
                string part = "";
                for (int i = 0; i < v.Length; i++)
                {
                    
                    if (v[i] == "DataSet")
                    {
                        part = "DataSet";
                    } else if (v[i] == "PlotSet")
                    {
                        part = "PlotSet";
                    } else
                    {
                        switch (part)
                        {
                            case "DataSet":
                                session.dataSet.Add(Data.Parse(v[i]));
                                break;
                            case "PlotSet":
                                session.plotSet.Add(Plot.Parse(v[i]));
                                break;
                        }
                    }
                }

                foreach (string item in v)
                {
                    if (part == "DataSet")
                    {

                    }
                }
                return true;
            }
            catch
            {
                session = null;
                return false;
            }
        }

        public override string ToString()
        {
            string text = "";
            text += "DataSet\n";
            foreach (Data data in dataSet)
            {
                text += data.ToString() + "\n";
            }
            text += "PlotSet\n";
            foreach (Plot plot in plotSet)
            {
                text += plot.ToString() + "\n";
            }
            return text;
        }
    }
}
