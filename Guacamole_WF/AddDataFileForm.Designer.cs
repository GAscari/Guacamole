﻿namespace Guacamole_WF
{
    partial class AddDataFileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddDataFileForm));
            this.chooseBtn = new System.Windows.Forms.Button();
            this.fileLabel = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.topNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.bottomNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.columnNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.separatorCBox = new System.Windows.Forms.ComboBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.singleRBtn = new System.Windows.Forms.RadioButton();
            this.multiRBtn = new System.Windows.Forms.RadioButton();
            this.columnTxt = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.topNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnNum)).BeginInit();
            this.SuspendLayout();
            // 
            // chooseBtn
            // 
            this.chooseBtn.Location = new System.Drawing.Point(12, 231);
            this.chooseBtn.Name = "chooseBtn";
            this.chooseBtn.Size = new System.Drawing.Size(75, 23);
            this.chooseBtn.TabIndex = 1;
            this.chooseBtn.Text = "Select file";
            this.chooseBtn.UseVisualStyleBackColor = true;
            this.chooseBtn.Click += new System.EventHandler(this.chooseBtn_Click);
            // 
            // fileLabel
            // 
            this.fileLabel.AutoSize = true;
            this.fileLabel.Location = new System.Drawing.Point(93, 236);
            this.fileLabel.Name = "fileLabel";
            this.fileLabel.Size = new System.Drawing.Size(78, 13);
            this.fileLabel.TabIndex = 2;
            this.fileLabel.Text = "no file selected";
            this.fileLabel.TextChanged += new System.EventHandler(this.file_TextChanged);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(168, 283);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 9;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(87, 283);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 10;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Rows to skip (top)";
            // 
            // topNum
            // 
            this.topNum.Location = new System.Drawing.Point(13, 30);
            this.topNum.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.topNum.Name = "topNum";
            this.topNum.Size = new System.Drawing.Size(230, 20);
            this.topNum.TabIndex = 12;
            this.topNum.ValueChanged += new System.EventHandler(this.file_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Rows to skip (bottom)";
            // 
            // bottomNum
            // 
            this.bottomNum.Location = new System.Drawing.Point(13, 74);
            this.bottomNum.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.bottomNum.Name = "bottomNum";
            this.bottomNum.Size = new System.Drawing.Size(230, 20);
            this.bottomNum.TabIndex = 14;
            this.bottomNum.ValueChanged += new System.EventHandler(this.file_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Column index (1, 2, 3...)";
            // 
            // columnNum
            // 
            this.columnNum.Location = new System.Drawing.Point(87, 120);
            this.columnNum.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.columnNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columnNum.Name = "columnNum";
            this.columnNum.Size = new System.Drawing.Size(156, 20);
            this.columnNum.TabIndex = 16;
            this.columnNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.columnNum.ValueChanged += new System.EventHandler(this.file_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Separator";
            // 
            // separatorCBox
            // 
            this.separatorCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.separatorCBox.FormattingEnabled = true;
            this.separatorCBox.Location = new System.Drawing.Point(13, 191);
            this.separatorCBox.Name = "separatorCBox";
            this.separatorCBox.Size = new System.Drawing.Size(230, 21);
            this.separatorCBox.TabIndex = 18;
            this.separatorCBox.SelectedIndexChanged += new System.EventHandler(this.file_TextChanged);
            // 
            // singleRBtn
            // 
            this.singleRBtn.AutoSize = true;
            this.singleRBtn.Checked = true;
            this.singleRBtn.Location = new System.Drawing.Point(12, 120);
            this.singleRBtn.Name = "singleRBtn";
            this.singleRBtn.Size = new System.Drawing.Size(54, 17);
            this.singleRBtn.TabIndex = 19;
            this.singleRBtn.TabStop = true;
            this.singleRBtn.Text = "Single";
            this.singleRBtn.UseVisualStyleBackColor = true;
            this.singleRBtn.CheckedChanged += new System.EventHandler(this.singleRBtn_CheckedChanged);
            // 
            // multiRBtn
            // 
            this.multiRBtn.AutoSize = true;
            this.multiRBtn.Location = new System.Drawing.Point(12, 144);
            this.multiRBtn.Name = "multiRBtn";
            this.multiRBtn.Size = new System.Drawing.Size(61, 17);
            this.multiRBtn.TabIndex = 20;
            this.multiRBtn.Text = "Multiple";
            this.multiRBtn.UseVisualStyleBackColor = true;
            this.multiRBtn.CheckedChanged += new System.EventHandler(this.singleRBtn_CheckedChanged);
            // 
            // columnTxt
            // 
            this.columnTxt.Enabled = false;
            this.columnTxt.Location = new System.Drawing.Point(87, 143);
            this.columnTxt.Name = "columnTxt";
            this.columnTxt.Size = new System.Drawing.Size(156, 20);
            this.columnTxt.TabIndex = 21;
            // 
            // AddDataFileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 319);
            this.Controls.Add(this.columnTxt);
            this.Controls.Add(this.multiRBtn);
            this.Controls.Add(this.singleRBtn);
            this.Controls.Add(this.separatorCBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.columnNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bottomNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.topNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.fileLabel);
            this.Controls.Add(this.chooseBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddDataFileForm";
            this.Text = "Add data from file";
            ((System.ComponentModel.ISupportInitialize)(this.topNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.columnNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button chooseBtn;
        private System.Windows.Forms.Label fileLabel;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown topNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown bottomNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown columnNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox separatorCBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.RadioButton singleRBtn;
        private System.Windows.Forms.RadioButton multiRBtn;
        private System.Windows.Forms.TextBox columnTxt;
    }
}