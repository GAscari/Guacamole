﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Guacamole_WF
{
    public partial class AddDataFileForm : Form
    {
        private List<int> index;
        private List<List<double>> dataList;
        private string path = "";
        bool multi;

        public List<List<double>> DataList { get => dataList; }

        public AddDataFileForm(bool multi)
        {
            this.multi = multi;
            InitializeComponent();
            separatorCBox.Items.Add("Tabulation");
            separatorCBox.Items.Add("Comma");
            separatorCBox.Items.Add("Point");
            separatorCBox.Items.Add("Semicolon");
            separatorCBox.Items.Add("Pipe");
            if (!multi)
            {
                multiRBtn.Enabled = false;
            }
        }

        private void file_TextChanged(object sender, EventArgs e)
        {
            int top, bottom, column;
            if (Int32.TryParse(columnNum.Value.ToString(), out column) && Int32.TryParse(bottomNum.Value.ToString(), out bottom) && Int32.TryParse(topNum.Value.ToString(), out top) && top > -1 && bottom > -1 && column > -1 && separatorCBox.SelectedIndex > -1 && path.Length > 0)
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            try
            {
                dataList = new List<List<double>>();
                StreamReader sr = new StreamReader(path);
                List<string> list = new List<string>();
                while (!sr.EndOfStream)
                {
                    list.Add(sr.ReadLine());
                }
                sr.Close();
                char separator = 'A';
                switch(separatorCBox.Text)
                {
                    case "Tabulation": separator = '\t'; break;
                    case "Comma": separator = ','; break;
                    case "Point": separator = '.'; break;
                    case "Pipe": separator = '|'; break;
                    case "Semicolon": separator = ';'; break;
                }
                index = new List<int>();
                if (singleRBtn.Checked)
                {
                    index.Add((int)columnNum.Value);
                    dataList.Add(new List<double>());
                } else
                {
                    string[] a = columnTxt.Text.Split(',');
                    foreach(string value in a)
                    {
                        index.Add(Int32.Parse(value.Trim()));
                        dataList.Add(new List<double>());
                    }
                }
                for (int i = (int)topNum.Value; i < list.Count - (int)bottomNum.Value; i++)
                {
                    string[] v = list[i].Split(separator);
                    for (int j = 0; j < index.Count; j++)
                    {
                        dataList[j].Add(Double.Parse(v[index[j] - 1]));
                    }
                }
                if (dataList.Count == 0) dataList = null;
            }
            catch
            {
                dataList = null;
            }
        }

        private void chooseBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
                string[] a = path.Split('\\');
                fileLabel.Text = a[a.Length - 1];
            }
        }

        private void singleRBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (singleRBtn.Checked)
            {
                columnNum.Enabled = true;
                columnTxt.Enabled = false;
            } else
            {
                columnNum.Enabled = false;
                columnTxt.Enabled = true;
            }
        }
    }
}
