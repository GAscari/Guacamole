﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Guacamole
{
    public partial class AppendDataListForm : Form
    {
        private DataSet _dataSet;
        private int _index = -1;
        private List<double> _list;
        public int Index { get => _index; }
        public List<double> List { get => _list; set => _list = value; }

        public AppendDataListForm(DataSet dataSet)
        {
            _dataSet = dataSet;
            InitializeComponent();
        }
                
        private void listCBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if ((fileRBtn.Checked || intervalRBtn.Checked) && listCBox.SelectedIndex > -1)
            {
                _index = listCBox.SelectedIndex;
                ok_btn.Enabled = true;
            } else
            {
                ok_btn.Enabled = false;
            }

        }

        private void ok_btn_Click(object sender, EventArgs e)
        {
            if (fileRBtn.Checked)
            {
                FileForm form = new FileForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    try
                    {
                        _list = new List<double>();
                        StreamReader sr = new StreamReader(form.FileName);
                        List<string> rows = new List<string>();
                        while (!sr.EndOfStream)
                        {
                            rows.Add(sr.ReadLine());
                        }
                        sr.Close();
                        for (int i = form.TopSkip; i < rows.Count - form.BottomSkip; i++)
                        {
                            string[] s = rows[i].Split(form.Separator);
                            _list.Add(double.Parse(s[form.Column].Replace('.', ',')));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        throw new Exception();
                    }
                }
            }
            else if (intervalRBtn.Checked)
            {
                IntervalForm form = new IntervalForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    _list = new List<double>();
                    for (int i = 0; i < form.Count; i++)
                    {
                        _list.Add(i * form.Step + form.Start);
                    }
                }
            }
        }

        private void AppendDataListForm_Load(object sender, EventArgs e)
        {
            foreach(DataList item in _dataSet)
            {
                listCBox.Items.Add(item.Name);
            }
        }
    }
}
