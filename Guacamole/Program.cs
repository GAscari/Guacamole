﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Guacamole
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new MainForm());
        }
    }
}
