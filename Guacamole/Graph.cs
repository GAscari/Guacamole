﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace Guacamole
{
    public class Plot
    {
        private string _name;
        private bool _visible;
        private Color _color;
        private List<string> _names;

        public string Name { get => _name; set => _name = value; }
        public bool Visible { get => _visible; set => _visible = value; }
        public Color Color { get => _color; set => _color = value; }
        public List<string> Names { get => _names; set => _names = value; }


        public List<double> GetList(string name, DataSet dataSet)
        {
            List<double> list = new List<double>();
            list = dataSet.Find(x => x.Name == name);
            return list;
        }

        public string ReadNames
        {
            get
            {
                string text = string.Empty;
                foreach (string name in _names)
                {
                    text += name + ",";
                }
                text = text.Substring(0, text.Length - 1);
                return text;
            }
        }

        public Plot()
        {

        }

        public Plot(string name, Color color, bool visible, List<string> names)
        {
            _name = name;
            _visible = visible;
            _color = color;
            _names = names;
        }

        public override string ToString()
        {
            string names = string.Empty;
            foreach(string name in _names)
            {
                names += name + ",";
            }
            names = names.Substring(0, names.Length - 1);
            return string.Format("{0};{1};{2};{3}", _name, _color.ToArgb(), _visible, names);
        }

        public static Plot Parse(string text)
        {
            Plot graph;
            try
            {
                string[] a = text.Split(';');
                string[] b = a[3].Split(',');
                List<string> names = new List<string>();
                foreach (string name in b)
                {
                    names.Add(name.Trim());
                }
                graph = new Plot(a[0], Color.FromArgb(int.Parse(a[1])), bool.Parse(a[2]), names);
                
            }
            catch (Exception e)
            {
                graph = null;
            }
            return graph;
        }
    }
}
