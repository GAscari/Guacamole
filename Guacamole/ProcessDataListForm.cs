﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole
{
    public partial class ProcessDataListForm : Form
    {
        DataSet _dataSet;
        DataList _dataList;
        public ProcessDataListForm(DataSet dataSet)
        {
            _dataSet = dataSet;
            InitializeComponent();
        }

        public DataList DataList { get => _dataList; }

        private void ProcessDataListForm_Load(object sender, EventArgs e)
        {
            foreach (DataList item in _dataSet)
            {
                listCBox.Items.Add(item.Name);
            }
            functionCBox.DataSource = Enum.GetValues(typeof(Function));
            functionCBox.SelectedIndex = -1;
        }

        private void content_Changed(object sender, EventArgs e)
        {
            okBtn.Enabled = false;
            if (functionCBox.SelectedIndex > -1 && listCBox.SelectedIndex > -1)
            {
                switch (Enum.Parse(typeof(Function), functionCBox.SelectedValue.ToString()))
                {
                    case Function.Average:
                        argLbl.Text = "Span";
                        argTxt.Enabled = true;
                        int span;
                        if (int.TryParse(argTxt.Text.Trim(), out span))
                        {
                            okBtn.Enabled = true;
                        }
                        break;
                    case Function.Derive:
                        argLbl.Text = "None";
                        argTxt.Enabled = false;
                        okBtn.Enabled = true;
                        break;
                }
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            switch (Enum.Parse(typeof(Function), functionCBox.SelectedValue.ToString()))
            {
                case Function.Average:
                    int span;
                    int.TryParse(argTxt.Text.Trim(), out span);
                    _dataList = DataList.AverageOn(_dataSet[listCBox.SelectedIndex], span);
                    break;
                case Function.Derive:
                    _dataList = DataList.Derive(_dataSet[listCBox.SelectedIndex]);
                    break;
                default:
                    _dataList = new DataList("arg", DataListFormat.N0);
                    break;
            }
        }
    }

    public enum Function
    {
        Average,
        Derive
    }
}
