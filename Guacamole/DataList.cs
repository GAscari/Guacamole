﻿using System.Collections.Generic;
using System.Drawing;
using System;
using System.IO;

namespace Guacamole
{
    public enum DataListFormat
    {
        N0,
        E1, E2, E3, E6, E9, E12,
        N1, N2, N3, N6, N9, N12
    }


    public class DataList : List<double>
    {
        private string _name;
        private DataListFormat _format;
        public string Name { get => _name; set => _name = value; }

        public double Average
        {
            get
            {
                double average = 0;
                foreach (double item in this)
                {
                    average = average + (item / this.Count);
                }
                return average;
            }
        }
        public double MaxValue
        {
            get {
                if (this.Count > 0)
                {
                    double max = this[0];
                    foreach (double item in this)
                    {
                        if (item > max) max = item;
                    }
                    return max;
                }
                else return 0;
            }
        }
        public double MinValue
        {
            get
            {
                if (this.Count > 0)
                {
                    double min = this[0];
                    foreach (double item in this)
                    {
                        if (item < min) min = item;
                    }
                    return min;
                }
                else return 0;
            }
        }

        public DataListFormat Format { get => _format; set => _format = value; }

        public DataList()
        {

        }

        public DataList(string name, DataListFormat format) : this()
        {
            _name = name;
            _format = format;
        }

        public DataList(string name, DataListFormat format, double start, double step, int count) : this(name, format)
        {
            for (int i = 0; i < count; i++)
            {
                Add(start + step * i);
            }
        }

        public static DataList Derive(DataList f1)
        {
            DataList f2 = new DataList(f1.Name + " derive", f1.Format);
            if (f1.Count > 1)
            {
                for (int i = 0; i < f1.Count - 1; i++)
                {
                    f2.Add(f1[i + 1] - f1[i]);
                }
                f2.Add(f2[f2.Count-1]);
            }
            return f2;

        }

        public static DataList AverageOn(DataList f1, int span)
        {
            if (span > f1.Count) span = f1.Count;
            DataList f2 = new DataList(f1.Name + " - aver. on " + span, f1.Format);
            if (f1.Count > span)
            {
                if (span % 2 == 1)
                {
                    for (int i = 0; i < (span + 1) / 2; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(0, i + (span + 1) / 2)));
                    }
                    for (int i = (span + 1) / 2; i < f1.Count - (span + 1) / 2; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(i - (span + 1) / 2, span)));
                    }
                    for (int i = f1.Count - (span + 1) / 2; i < f1.Count; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(i - (span + 1) / 2, (span + 1) / 2)));
                    }
                }
                else
                {
                    for (int i = 0; i < span / 2; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(0, i + span/2)));
                    }
                    for (int i = span/2; i < f1.Count - span/2; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(i - span / 2, span)));
                    }
                    for (int i = f1.Count - span / 2; i < f1.Count; i++)
                    {
                        f2.Add(AverageOnList(f1.GetRange(i - span / 2, span / 2)));
                    }
                }
            }
            return f2;
        }

        private static double AverageOnList(List<double> list)
        {
            double average = 0;
            foreach(double item in list)
            {
                average += item / list.Count;
            }
            return average;
        }

        public static DataList Parse(string text)
        {
            DataList data;
            try
            {
                string[] a = text.Split(';');
                string[] b = a[2].Split(',');
                List<double> list = new List<double>();
                data = new DataList(a[0], (DataListFormat)int.Parse(a[1]));
                foreach (string item in b)
                {
                    data.Add(double.Parse(item.Replace('.', ',')));
                }
            } catch
            {
                data = null;
                
            }
            return data;
        }
        public override string ToString()
        {
            string text = string.Format("{0};{1};", _name, (int)_format);
            foreach (double item in this)
            {
                text += item.ToString().Replace(',', '.') + ",";
            }
            text = text.Substring(0, text.Length - 1);
            return text;
        }
    }
}
