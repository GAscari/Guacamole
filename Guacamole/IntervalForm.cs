﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole
{
    public partial class IntervalForm : Form
    {
        private double _start;
        private double _step;
        private int _count;



        public IntervalForm()
        {
            InitializeComponent();
        }

        public double Start { get => _start;}
        public double Step { get => _step;}
        public int Count { get => _count;}

        private void interval_TextChanged(object sender, EventArgs e)
        {
            if (countNum.Value > 0)
            {
                okBtn.Enabled = true;
            }
            else okBtn.Enabled = false;
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            _start = (double)startNum.Value;
            _step = (double)stepNum.Value;
            _count = int.Parse(countNum.Value.ToString());
        }

        /*private void colorBtn_Paint(object sender, PaintEventArgs e)
        {
            var w = 16;
            var h = 13;
            var x = e.ClipRectangle.Left + (e.ClipRectangle.Width - w) / 2;
            var y = e.ClipRectangle.Top + (e.ClipRectangle.Height - h) / 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
            e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
            e.Graphics.FillRectangle(new SolidBrush(colorDialog.Color), x + 2, y + 2, w - 4, h - 4);
        }*/
    }
}
