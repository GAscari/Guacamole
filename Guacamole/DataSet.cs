﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole
{
    public class DataSet : List<DataList>
    {
        private string _oldName;

        public string OldName { get => _oldName; set => _oldName = value; }

        public DataSet()
        {

        }

        public override string ToString()
        {
            string text = string.Empty;
            foreach(DataList item in this)
            {
                text +=  item.ToString() + "\n";
            }
            return text;
        }

        public int GetMaxCount()
        {
            int max = 0;
            foreach (DataList dataList in this)
            {
                if (dataList.Count > max) max = dataList.Count;
            }
            return max;
        }

        public DataList GetDataListFromName(string name)
        {
            return this.Find(x => x.Name == name);
        }

        public bool FindOtherNames(string name)
        {
            bool other = true;
            int pos1 = this.FindIndex(x => x.Name == name);
            int pos2 = this.FindLastIndex(x => x.Name == name);
            if (pos1 == pos2) other = false;
            return other;
        }

        public bool YetPresentName(string name)
        {
            bool yet = false;
            if (this.FindIndex(x => x.Name == name) > -1) yet = true;
            return yet;
        }

        public static DataSet Parse(string text)
        {
            DataSet dataSet;
            try
            {
                string[] a = text.Split('\n');
                dataSet = new DataSet();
                for (int i = 0; i < a.Length; i++)
                {
                    dataSet.Add(DataList.Parse(a[i]));
                }
            } catch
            {
                dataSet = null;
            }
            return dataSet;
        }
    }
}
