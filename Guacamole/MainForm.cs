﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Guacamole
{
    public partial class MainForm : Form
    {
        DataSet dataSet = new DataSet();
        BindingSource bindingSourceInfo = new BindingSource();
        GraphSet graphSet = new GraphSet();
        BindingSource bindingSourceGraph = new BindingSource();
        int unit = 100;


        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tableDGV_Update(sender, e);
            infoDGV_Update(sender, e);
            graphDGV_Update(sender, e);
            //origin = new Point(graphBox.Width / 2, graphBox.Height / 2);
            graphBox_Update(sender, e);
        }

        private void graphDGV_Update(object sender, EventArgs e)
        {
            bindingSourceGraph.DataSource = typeof(GraphSet);
            bindingSourceGraph.DataSource = graphSet;
            graphDGV.DataSource = typeof(BindingSource);
            graphDGV.DataSource = bindingSourceGraph;
            graphDGV.Columns.Clear();
            graphDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            graphDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "ReadNames", Name = "Used lists", MinimumWidth = 80 });
            graphDGV.Columns.Add(new DataGridViewCheckBoxColumn() { DataPropertyName = "Visible", Name = "Visible", MinimumWidth = 80 });
            graphDGV.Columns.Add(new DataGridViewTextBoxColumn() { Name = "Color", MinimumWidth = 80 });
        }
        private void infoDGV_Update(object sender, EventArgs e)
        {
            bindingSourceInfo.DataSource = typeof(DataSet);
            bindingSourceInfo.DataSource = dataSet;
            infoDGV.DataSource = typeof(BindingSource);
            infoDGV.DataSource = bindingSourceInfo;
            infoDGV.Columns.Clear();
            infoDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            infoDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Count", Name = "Count", MinimumWidth = 80 });
            infoDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "MaxValue", Name = "Max. value", MinimumWidth = 80 });
            infoDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "MinValue", Name = "Min. value", MinimumWidth = 80 });
            infoDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Average", Name = "Average", MinimumWidth = 80 });
            infoDGV.Columns.Add(new DataGridViewComboBoxColumn() { DataPropertyName = "Format", Name = "Format", MinimumWidth = 80, DataSource = Enum.GetValues(typeof(DataListFormat)), DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox });
        }
        private void infoDGV_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex > 1 && e.ColumnIndex < 5 && dataSet.Count > 0)
            {
                infoDGV.Columns[e.ColumnIndex].DefaultCellStyle.Format = "E3";
                infoDGV.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            } else if (e.ColumnIndex == 5 && dataSet.Count > 0)
            {
                infoDGV.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
        }
        private void tableDGV_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataSet.Count > 0) {
                dataDGV.Columns[e.ColumnIndex].DefaultCellStyle.Format = dataSet[e.ColumnIndex].Format.ToString();
                dataDGV.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }
        private void tableDGV_Update(object sender, EventArgs e)
        {
            dataDGV.Columns.Clear();
            dataDGV.Rows.Clear();
            for (int i = 0; i < dataSet.Count; i++)
            {
                dataDGV.Columns.Add(dataSet[i].Name, dataSet[i].Name);
                dataDGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataDGV.Columns[i].MinimumWidth = 80;
                //dataDGV.Columns[i].DefaultCellStyle.Format = dataSet[i].Format.ToString();
            }
            if (dataSet.GetMaxCount() > 0)
            {
                dataDGV.Rows.Add(dataSet.GetMaxCount());
                for (int i = 0; i < dataDGV.Rows.Count; i++)
                {
                    dataDGV.Rows[i].HeaderCell.Value = (i + 1).ToString();
                }
                for (int i = 0; i < dataSet.Count; i++)
                {
                    for (int j = 0; j < dataSet[i].Count; j++)
                    {
                        dataDGV[i, j].Value = dataSet[i][j];
                    }
                }
            }
        }

        private void saveTSBtn_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                sw.WriteLine("DataSet");
                foreach (DataList item in dataSet)
                {
                    sw.WriteLine(item);
                }
                sw.WriteLine("Graph");
                foreach (Plot item in graphSet)
                {
                    sw.WriteLine(item);
                }
                sw.Close();
            }
        }
        private void loadTSBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                dataSet = new DataSet();
                graphSet = new GraphSet();
                try
                {
                    dataDGV.Columns.Clear();
                    dataDGV.Rows.Clear();

                    if (sr.ReadLine() == "DataSet")
                    {
                        string s = sr.ReadLine();
                        do
                        {
                            dataSet.Add(DataList.Parse(s));
                            s = sr.ReadLine();
                        } while (s != "Graph");
                        while (!sr.EndOfStream)
                        {
                            graphSet.Add(Plot.Parse(sr.ReadLine()));
                        }
                    }
                    foreach (DataList item in dataSet)
                    {
                        if (dataSet.FindOtherNames(item.Name)) throw new Exception();
                    }
                }
                catch
                {
                    dataSet = new DataSet();
                    Console.WriteLine("An error occurred during loading GDataSet", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                sr.Close();
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
                graphDGV_Update(sender, e);
                graphBox_Update(sender, e);
            }
        }

        private void infoDGV_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (infoDGV.SelectedCells[0].ColumnIndex == 5)
            {
                infoDGV.EndEdit();
                dataDGV.Refresh();
            }
        }
        private void infoDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (dataSet.FindOtherNames(dataSet[e.RowIndex].Name))
                {
                    dataSet[e.RowIndex].Name += " *";
                }
                graphSet.ChangeDataListNames(dataSet.OldName, dataSet[e.RowIndex].Name);
                tableDGV_Update(sender, e);
                graphDGV_Update(sender, e);
            } else if (e.ColumnIndex == 5)
            {
                dataDGV.Invalidate();
            }
        }
        private void graphDGV_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex < 0)
            {

            }
            else if (e.ColumnIndex == 3)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = 20;
                var h = 13;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
                e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
                e.Graphics.FillRectangle(new SolidBrush(graphSet[e.RowIndex].Color), x + 2, y + 2, w - 4, h - 4);

                e.Handled = true;
            }
        }
        private void graphDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {

            }
            else if (e.ColumnIndex == 2)
            {
                //graphDGV.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
            else if (e.ColumnIndex == 3)
            {
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    graphSet[e.RowIndex].Color = colorDialog.Color;
                    graphDGV_Update(sender, e);
                    graphBox_Update(sender, e);
                }
            }
        }
        private void graphDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (graphSet.FindOtherNames(graphSet[e.RowIndex].Name))
            {
                graphSet[e.RowIndex].Name += " *";
            }
        }
        private void infoDGV_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                dataSet.OldName = dataSet[e.RowIndex].Name;
            }
        }

        private void newTSBtn_Click(object sender, EventArgs e)
        {
            try
            {
                NewDataListForm form = new NewDataListForm(dataSet);
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    dataSet.Add(form.DataList);
                    tableDGV_Update(sender, e);
                    infoDGV_Update(sender, e);
                } else if (form.DialogResult == DialogResult.Cancel)
                { 

                }
            } catch (Exception ex)
            {
                Console.WriteLine("Error occurred during loading. The specified file sintax is probably different from the real one. Please check and retry.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void appendTSBtn_Click(object sender, EventArgs e)
        {
            AppendDataListForm form = new AppendDataListForm(dataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                dataSet[form.Index].AddRange(form.List);
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
            }
        }
        private void graph2dTSBtn_Click(object sender, EventArgs e)
        {
            NewGraphForm form = new NewGraphForm(dataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                graphSet.Add(form.Graph);
                graphDGV_Update(sender, e);
            }
        }

        private void dataList_Delete(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you really sure to delete this list?", "Delete list", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                graphSet.DeleteDataList(dataSet[infoDGV.SelectedRows[0].Index].Name);
                dataSet.RemoveAt(infoDGV.SelectedRows[0].Index);
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
                graphDGV_Update(sender, e);
            }
        }
        private void dataList_MoveUp(object sender, EventArgs e)
        {
            int index = infoDGV.SelectedRows[0].Index;
            if (index > 0)
            {
                DataList list = dataSet[index];
                dataSet[index] = dataSet[index - 1];
                dataSet[index - 1] = list;
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
                infoDGV.ClearSelection();
                infoDGV.Rows[index - 1].Selected = true;
            }
        }
        private void dataList_MoveDown(object sender, EventArgs e)
        {
            int index = infoDGV.SelectedRows[0].Index;
            if (index < infoDGV.Rows.Count - 1)
            {
                DataList list = dataSet[index];
                dataSet[index] = dataSet[index + 1];
                dataSet[index + 1] = list;
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
                infoDGV.ClearSelection();
                infoDGV.Rows[index + 1].Selected = true;
            }
        }
        private void infoDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && e.RowIndex >= 0 && e.RowIndex < dataSet.Count)
            {
                infoDGV.ClearSelection();
                infoDGV.Rows[e.RowIndex].Selected = true;

                MenuItem[] menuItems = new MenuItem[]
                {
                    new MenuItem("Delete", dataList_Delete),
                    new MenuItem("Move up", dataList_MoveUp),
                    new MenuItem("Move down", dataList_MoveDown)
                };
                ContextMenu contextMenu = new ContextMenu(menuItems);
                contextMenu.Show(this, PointToClient(MousePosition));
            }
            else if (e.Button == MouseButtons.Left && e.RowIndex >= 0 && e.RowIndex < dataSet.Count && e.ColumnIndex == 5)
            {
                infoDGV.BeginEdit(true);
                ((ComboBox)infoDGV.EditingControl).DroppedDown = true;
            }

            
        }

        private void editTSBtn_Click(object sender, EventArgs e)
        {
            ProcessDataListForm form = new ProcessDataListForm(dataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                dataSet.Add(form.DataList);
                tableDGV_Update(sender, e);
                infoDGV_Update(sender, e);
            }
        }



        private void graphBox_Update(object sender, EventArgs e)
        {
            Bitmap area = new Bitmap(graphBox.Size.Width, graphBox.Size.Height);
            Graphics graphics = Graphics.FromImage(area);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            int space = 30;
            Pen axis = new Pen(Color.Gray);
            Pen blue = new Pen(Color.Blue);
            
            
            //if (Point(origin).Y >= 0 && Point(origin).Y < graphBox.Height) graphics.DrawLine(axis, 0, Point(origin).Y, graphBox.Width-1, Point(origin).Y);

            

            foreach (Plot graph in graphSet)
            {
                if (graph.Visible)
                {
                    Pen pen = new Pen(graph.Color);
                    pen.Width = 3;
                    List<double> X = dataSet.GetDataListFromName(graph.Names[0]);
                    List<double> Y = dataSet.GetDataListFromName(graph.Names[1]);
                    for (int i = 0; i < Math.Min(X.Count, Y.Count) * unit; i ++)
                    {
                        graphics.DrawEllipse(pen, space + (float)X[i / unit], graphBox.Height - (float)Y[i / unit] - space, 1, 1);
                    }
                }
            }

            graphBox.Image = area;
            graphics.Dispose();
            GC.Collect();    
        }

        private void splitContainer_SplitterMoved(object sender, SplitterEventArgs e)
        {
            graphBox_Update(sender, e);
        }

        private void splitContainer_SizeChanged(object sender, EventArgs e)
        {
            graphBox_Update(sender, e);
        }
    }
}
